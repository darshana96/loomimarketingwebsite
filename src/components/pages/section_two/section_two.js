import React from 'react'

// require('./section_two.less')

class SectionTwo extends React.Component {
	render() {
		return (
			<div className="section" id="section1">
      			<div className="slide active">
      			        <div className="intro">
      			          <h1>SectionTwo Page One</h1>
      			          <p>Not only vertical scrolling but also horizontal scrolling. With fullPage.js you will be able to add horizontal sliders in the most simple way ever.
      			          </p>
      			        </div>
      			 </div>
				<div className="slide" id="slide2"><h1>Even inside slides</h1></div>
				<div className="slide" id="slide3">
					<div className="intro">
						<h1>Scrolling slide</h1>
						<img src="./imgs/iphone-red.png" alt="iphone" id="iphone-two" />
						<p>
							Eu nec ferri molestie consequat, vel at alia dolore putant. Labore philosophia ut vix. In vis nostrud interesset appellantur, vis et tation feugiat scripserit. Te nec noster suavitate persecuti. Diceret erroribus cu vix, alii omnes ei sit. Sea an corrumpit patrioque, virtute accumsan nominavi et usu, ex mei dolore vocibus incorrupte.

							Duo ea saperet tacimates. Sed possim prodesset no, per novum putent doctus ea. Eu mea magna aliquip graecis, pri corpora officiis complectitur ei, lorem saepe consetetur his ad. Meis consulatu ei vis, an altera ocurreret interesset qui.

							Eu ponderum comprehensam his, case antiopam pri te. Mel ne partem consequat instructior. Ad dicunt malorum sea, ex qui omnes invenire gubergren. Ius cu autem aliquando, pri vide ornatus perpetua an, no has epicuri verterem. Nam at animal pertinax efficiantur.
							Eu ponderum comprehensam his, case antiopam pri te. Mel ne partem consequat instructior. Ad dicunt malorum sea, ex qui omnes invenire gubergren. Ius cu autem aliquando, pri vide ornatus perpetua an, no has epicuri verterem. Nam at animal pertinax efficiantur.
							Eu ponderum comprehensam his, case antiopam pri te. Mel ne partem consequat instructior. Ad dicunt malorum sea, ex qui omnes invenire gubergren. Ius cu autem aliquando, pri vide ornatus perpetua an, no has epicuri verterem. Nam at animal pertinax efficiantur.

							Per alienum torquatos eu.
						</p>
					</div>
      			   </div>
    		</div>
		) 
	}
}

export default SectionTwo