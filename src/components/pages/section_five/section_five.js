import React from 'react'


class SectionFive extends React.Component {
    render() {
        return (
            <div className="section" id="section4">
                <div class="intro">
                    <h1>No limitations!</h1>
                    <p>Content is a priority. Even if it is so large :)</p>
                </div>
            </div>
        )
    }
}

export default SectionFive