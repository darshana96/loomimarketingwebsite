import React from 'react'

import Nav from './nav/nav'
import SectionOne from './pages/section_one/section_one'
import SectionTwo from './pages/section_two/section_two'
import SectionThree from './pages/section_three/section_three'
import SectionFour from './pages/section_four/section_four'
import SectionFive from './pages/section_five/section_five'

class App extends React.Component {
	constructor() {
		super()
	}

	componentDidMount() {
		$('#fullpage').fullpage({
            sectionsColor: ['#4A6FB1', '#939FAA', '#323539','#4A6FB1','#939FAA'],
            anchors: ['firstPage', 'secondPage', '3rdPage', '4thpage', '5thpage'],
            menu: '#menu',
            css3: true,
            scrollingSpeed: 1000
        });
	}
 	render() {
 		return (
 			<div>
 				<Nav></Nav>
 				<div id='fullpage'>
 					<SectionOne></SectionOne>
 					<SectionTwo></SectionTwo>
 					<SectionThree></SectionThree>
 					<SectionFour></SectionFour>
					<SectionFive></SectionFive>
 				</div>
 			</div>
 		)
 	}
}

export default App