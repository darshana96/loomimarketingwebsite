import React from 'react'

require('./nav.less')

class Nav extends React.Component {
  render () {
    return (
      <ul id="menu">
        <li data-menuanchor="firstPage" class="active">
          <a href="#firstPage">About</a>
        </li>
        <li data-menuanchor="secondPage">
          <a href="#secondPage">What it does</a>
        </li>
        <li data-menuanchor="3rdPage">
          <a href="#3rdPage">How it works</a>
        </li>
        <li data-menuanchor="4thpage">
          <a href="#4thpage">Use Cases</a>
        </li>
         <li data-menuanchor="5thpage">
              <a href="#5thpage">Where it works</a>
         </li>
      </ul>
    )
  }
}

export default Nav
