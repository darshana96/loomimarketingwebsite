import React from 'react'
import {render} from 'react-dom'
import {createStore,applyMiddleware} from 'redux'
import {Provider} from 'react-redux'
import promiseMiddleware from 'redux-promise';
import rootReducer from './reducer'
import App from './components/app'
import '../src/assets/css/index.css'
// import './base.less'

const store = createStore(rootReducer,applyMiddleware(promiseMiddleware));

render(
    <Provider store={store}>
            <App />
    </Provider>,
    document.getElementById('app'));

